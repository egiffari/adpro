package id.ac.ui.cs.advprog.tutorial1.strategy.core;

public class AttackWithMagic implements AttackBehavior {
        //ToDo: Complete me

    @Override
    public String attack() {
        return "Destroy your opponent's body and soul with the ultimate magic spell";
    }

    @Override
    public String getType() {

        return "Ultima spell";
    }
}
