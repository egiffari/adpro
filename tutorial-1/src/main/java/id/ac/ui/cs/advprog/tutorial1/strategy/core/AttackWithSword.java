package id.ac.ui.cs.advprog.tutorial1.strategy.core;

public class AttackWithSword implements AttackBehavior {
        //ToDo: Complete me

    @Override
    public String attack() {
        return "Slash your opponent into pieces with the ultimate sword combo";
    }

    @Override
    public String getType() {

        return "Omnislash";
    }
}
