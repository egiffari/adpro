package id.ac.ui.cs.advprog.tutorial1.observer.core;

import java.util.List;

public class MysticAdventurer extends Adventurer {

        public MysticAdventurer(Guild guild) {
                this.name = "Mystic";
                //ToDo: Complete Me
                this.guild = guild;
                this.guild.add(this);
        }

        //ToDo: Complete Me

        @Override
        public void update() {

                List<Quest> questList = this.getQuests();
                Quest quest = this.guild.getQuest();
                String questType = this.guild.getQuestType();

                if(questType.equals("D") || questType.equals("E")){
                        questList.add(quest);
                }

        }
        //ToDo: Complete Me
}
